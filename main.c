#include "main.h"

void print_number_factors(int number)
{
    printf("Number factors: ");
    int divisor = 2;
    while(number != 1)
    {
        if (number%divisor == 0)
        {
            printf("%d ", divisor);
            number /= divisor;
        }
        else 
            divisor += 1;
    }
    printf("\n");
}

int main(int argc, char* argv[])
{
    int number;
    printf("Enter number:\n");
    scanf_s("%d", &number);
    print_number_factors(number);
    
    getch();
    return 0;

    
}